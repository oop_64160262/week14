/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chachai.swingprojrct;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class ReadFriend {

    public static void main(String[] args) {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("friend.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            ArrayList<Friend>friend = (ArrayList<Friend>) ois.readObject();
            System.out.println(friend);
            fis.close();
        } catch (FileNotFoundException ex) { // when can not found file
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) { // when read can not found class
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
